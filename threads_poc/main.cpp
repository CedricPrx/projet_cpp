// thread example
#include <iostream>       // std::cout
#include <thread>         // std::thread
#include<unistd.h>
#include <fstream>
#include <iostream>
#include "../contacts/contact.hpp"

using namespace std;

bool c = true;

void foo()
{
  while(c){
  	cout << "Foo" << endl;
       	sleep(1);
  }
  cout << "Finished Foo " << endl;
}

void bar(int x)
{
  // do stuff...
}

void readWrite(){

   ofstream outfile;
   outfile.open("afile.dat");

   char data[100];

   cout << "Writing to the file" << endl;
   cout << "Enter your name: ";

   cin.getline(data, 100);
   // write inputted data into the file.
   outfile << data << endl;

   cout << "Enter your age: ";
   cin >> data;
   cin.ignore();

   // again write inputted data into the file.
   outfile << data << endl;

   // close the opened file.
   outfile.close();

   // open a file in read mode.
   ifstream infile;
   infile.open("afile.dat");

   cout << "Reading from the file" << endl;
   infile >> data;

   // write the data at the screen.
   cout << data << endl;

   // again read the data from the file and display it.
   infile >> data;
   cout << data << endl;

   // close the opened file.
   infile.close();
}

typedef struct _ctype
    : std::ctype<char>
{
    typedef std::ctype<char> base;
    static base::mask const* make_table(unsigned char space,
                                        base::mask* table)
    {
        base::mask const* classic(base::classic_table());
        std::copy(classic, classic + base::table_size, table);
        table[space] |= base::space;
        return table;
    }
    _ctype(unsigned char space)
    : base(make_table(space, table))
    {
    }
    base::mask table[base::table_size];
} _ctype;


void saisieContact(){

	cout << "Veuillez saisir les informations du contact a ajouter" << endl;


	int idContact = 666;
	char *nom = "Diable";
	char *prenom = "Mr";
	char *sexe = "M";
	char *entreprise = "L'enfer";
	char *adresse = "15, Volcano St";
	char *complement = "BozoLand";
	char *code_postal = "66666";
	char *ville = "Orlando";
	char *email = "jesuisfou@hotmail.com";
	char* date_naissance = "01-01-1988";
	/*
	int idContact;
	char nom[STR_NAME_SZ];
	char prenom[STR_NAME_SZ];
	char sexe;
	char entreprise[STR_NAMENT_SZ];
	char adresse[STR_RUE_SZ];
	char complement[STR_COMPLT_SZ];
	char code_postal[INT_CP_SZ];
	char ville[STR_VILLE_SZ];
	char email[255];
	char date_naissance[10];

	cout << "IdContact: " << endl;
	cin >> idContact;

	cout << "Nom: " << endl;
	cin >> nom;
	cout << "Prenom:" << endl;
	cin >> prenom;
	cout << "Sexe" << endl;
	cin >> sexe;
	cout << "Entreprise" <<endl;
   	cin >> entreprise;
	cout << "Adresse" << endl;
	cin >> adresse;
	cout << "Complement": << endl;
	cin >> complement;
	cout << "Code Postal" << endl;
	cin >> code_postal;
	cout << "Ville"
	cin >> ville ;
	cout << "Email" << endl;
	cin >> email;
	cout << "Date Naissance" << endl;
	cin >> date_naissance;
	*/
	ofstream outfile;
   	outfile.open("afile.dat");

   	outfile << nom << "|"
	        << prenom << "|"
		<< sexe << "|"
		<< entreprise << "|"
		<< adresse << "|"
		<< complement << "|"
		<< code_postal << "|"
		<< ville << "|"
		<< email << "|"
		<< date_naissance << endl;


   	// close the opened file.
   	outfile.close();
	char data[400];

   	// open a file in read mode.
   	ifstream infile;
   	infile.open("afile.dat");

   	cout << "Reading from the file" << endl;

	std::locale global;
	std::locale loc(global, new _ctype('|'));
    	infile.imbue(loc);

	infile >> data;

   	// write the data at the screen.
   	cout << data << endl;

   	// again read the data from the file and display it.

   	// close the opened file.
   	infile.close();

}


void thread_test(){
  std::thread first (foo);     // spawn new thread that calls foo()
  //std::thread second (bar,0);  // spawn new thread that calls bar(0)

  std::cout << "main, foo and bar now execute concurrently...\n";

  // synchronize threads:
  char choice;
  while (c){
  	cout << "Continue? (Y/N)" << endl;
	cin >> choice;
	c = (choice != 'N');
  }

  first.join();                // pauses until first finishes

  //second.join();               // pauses until second finishes

  std::cout << "foo and bar completed.\n";
}
int main()
{
	//readWrite();
	saisieContact();
  	return 0;
}
