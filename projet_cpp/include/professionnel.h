//
//  professionnel.hpp
//  project_contacts
//
//  Created by Oussama Boukh on 29/12/2022.
//  Copyright © 2022 Oussama Boukh. All rights reserved.
//

#ifndef professionnel_h
#define professionnel_h
#include <iostream>
#include <string>
#include <stdio.h>
#include <sstream>

#include "contact.h"

class professionnel : public contact

{

public:
    professionnel();
    professionnel(int,string,string,SEXE,string,string,ADRESSE);

    ~professionnel();

    string getmail();
    string getnomEnt();
    ADRESSE getadr();

    void setmail(string);
    void setnomEnt(string);
    void setadr(ADRESSE);
    string affiche();
    void file_export();
    virtual void saisie(int id) override;



private:
    ADRESSE adr_ent;
    string mail;
    string nomEnt;



};



#endif /* professionnel_hpp */
