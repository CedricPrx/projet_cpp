#ifndef TEST_H
#define TEST_H

#include "sqlite3.h"


int insert_req_params_test(sqlite3 *db);

//Prototypes fonctions test Hierarchie classes
void classes_test();
void annuaire_test();
void test_hierarchie();

#endif // TEST_H
