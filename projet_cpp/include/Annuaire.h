#ifndef ANNUAIRE_H
#define ANNUAIRE_H

#include "contact.h"
#include <vector>

class Annuaire
{
    public:
        Annuaire(){};
        ~Annuaire();

        void ajouter(contact*);
        void affiche();


    protected:

    private:
        vector<contact*> liste_contact;

};


#endif // ANNUAIRE_H
