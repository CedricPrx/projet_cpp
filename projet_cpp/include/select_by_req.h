#ifndef select_by_req_h
#define select_by_req_h
#include "sqlite3.h"

int select_by_req(sqlite3 *db, const char *ville, const char *name);

#endif
