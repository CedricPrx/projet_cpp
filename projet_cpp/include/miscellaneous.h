#ifndef miscellaneous_h
#define miscellaneous_h

#include "sqlite3.h"
#include <string>
#include <locale>
#include "contact.h"

using namespace std;


typedef struct _ctype
    : std::ctype<char>
{
    typedef std::ctype<char> base;
    static base::mask const* make_table(unsigned char space,
                                        base::mask* table)
    {
        base::mask const* classic(base::classic_table());
        std::copy(classic, classic + base::table_size, table);
        table[space] |= base::space;
        return table;
    }
    _ctype(unsigned char space)
    : base(make_table(space, table))
    {
    }
    base::mask table[base::table_size];
} _ctype;


//Prototypes fonctions de conversion
string bd_string(int, int, int);
string concat (int, ADRESSE );
extern string conv_c2s(const unsigned char*);

//Prototypes fonctions test Hierarchie classes
void test_classes(void);
void test_annuaire(void);
void test_echange_db(void);

//Prototype fonctions sqlite3

sqlite3* open_db(string);
void close_db(sqlite3*, bool throwit=true);
//int insert_req (sqlite3*);
//int delete_req_params(sqlite3*, int);
//int select_req (sqlite3*);
//int select_by_req(sqlite3*, const char*, const char*);

//Prototypes test sqlite3
int insert_req_params_test(sqlite3*);


#endif
