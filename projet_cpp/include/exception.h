#ifndef exception_h
#define exception_h
#include <iostream>
#include <exception>
#include <string>


using namespace std;

enum CauseErreurs { ERR_ID, ERR_NAME, ERR_ADR, ERR_CP, ERR_BD, ERR_NAMENT, ERR_MAIL,        // erreurs parametres
                    ERR_FILE,                                                               // erreurs csv
                    ERR_OPEN, ERR_PREPARE, ERR_BIND, ERR_EXEC, ERR_FINALIZE, ERR_CLOSE,     // erreurs api sqlite
                  };

class Exception: public exception
{
    public:
        Exception(CauseErreurs errCause)
        {
            switch(errCause)
            {
                 case CauseErreurs::ERR_ID:
                    this->cause = "Erreur : entrer identifiant non nul et limite a 5 chiffres positifs.";
                    break;

                case CauseErreurs::ERR_NAME:
                    this->cause = "Erreur : entrer nom / prenom limite a 30 caracteres.";
                    break;

                case CauseErreurs::ERR_ADR:
                    this->cause = "Erreur : entrer adresse / complement d'adresse limite a 250 caracteres.";
                    break;

                case CauseErreurs::ERR_CP:
                    this->cause = "Erreur : entrer code postal limite a 5 chiffres.";
                    break;

                case CauseErreurs::ERR_BD:
                    this->cause = "Erreur : date de naissance invalide.";
                    break;

                case CauseErreurs::ERR_NAMENT:
                    this->cause = "Erreur : entrer nom de l'entreprise / limite a 50 caracteres.";
                    break;

                case CauseErreurs::ERR_MAIL:
                    this->cause = "Erreur : adresse mail invalide.";
                    break;

                case CauseErreurs::ERR_FILE:
                    this->cause = "Erreur : pb fichier csv";
                    break;

                default:
                    this->cause = "Autre Erreur";
                    break;
            }
        }

        Exception(CauseErreurs errCause, const char* sqlite3_errmsg)
        {
            switch(errCause)
            {
                case CauseErreurs::ERR_OPEN:
                    this->cause = "Erreur API SQLite3 OPEN: " + string(sqlite3_errmsg);
                    break;

                case CauseErreurs::ERR_PREPARE:
                    this->cause = "Erreur API SQLite3 PREPARE: " + string(sqlite3_errmsg);
                    break;

                case CauseErreurs::ERR_BIND:
                    this->cause = "Erreur API SQLite3 BIND: " + string(sqlite3_errmsg);                    break;
                    break;

                case CauseErreurs::ERR_EXEC:
                    this->cause = "Erreur API SQLite3 EXEC: " + string(sqlite3_errmsg);
                    break;

                case CauseErreurs::ERR_FINALIZE:
                    this->cause = "Erreur API SQLite3 FINALIZE: " + string(sqlite3_errmsg);
                    break;

                case CauseErreurs::ERR_CLOSE:
                    this->cause = "Erreur API SQLite3 CLOSE: " + string(sqlite3_errmsg);
                    break;

                default:
                    this->cause = "Erreur API SQLite3 NC: " + string(sqlite3_errmsg);
                    break;
            }
        }


        const char* what() const noexcept
        {
            return this->cause.c_str();
        }

    private:
        string cause;
};


#endif /* exception_hpp */

