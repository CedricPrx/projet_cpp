//
//  contact.hpp
//  project_contacts
//
//  Created by Oussama Boukh on 29/12/2022.
//  Copyright © 2022 Oussama Boukh. All rights reserved.
//
#ifndef contact_h
#define contact_h

#include <iostream>
#include <string>
#include<iomanip>
#include <fstream>

#define INT_ID_SZ 5
#define STR_NAME_SZ 31
#define STR_VILLE_SZ 101
#define INT_CP_SZ 5
#define STR_RUE_SZ 251
#define STR_COMPLT_SZ 251
#define STR_NAMENT_SZ 51



typedef enum SEXE {M=77,F=70}SEXE;

using namespace std;

typedef struct ADRESSE
{
    string Rue;
    string Complt;
    int CP;
    string Ville;

}ADRESSE;


class contact{


private:

    int id;
    string nom;
    string prenom;
    SEXE sexe;

public:

    contact();
    contact (int,string,string, SEXE);
    virtual ~contact();

    int getid();
    SEXE getsexe();
    string getnom();
    string getprenom();

    void setid(int);
    void setsexe(SEXE);
    void setnom(string);
    void setprenom(string);

    virtual string affiche()=0;
    virtual void file_export()=0;

    virtual void saisie(int id);
};



ostream& operator<<(ostream&, const ADRESSE&);


#endif /* contact_hpp */
