#ifndef insert_req_h
#define insert_req_h

#include "sqlite3.h"

int insert_req_params(
		sqlite3 *db,
		int idContact,
		const char *nom,
		const char *prenom,
		const char *sexe,
		const char *entreprise,
		const char *adresse,
		const char *complement,
		const char *code_postal,
		const char *ville,
		const char *email,
		const char *date_naissance);

#endif
