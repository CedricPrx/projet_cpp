#ifndef delete_req_h
#define delete_req_h

#include "sqlite3.h"

int delete_req_params(sqlite3 *db, int idContact);

#endif
