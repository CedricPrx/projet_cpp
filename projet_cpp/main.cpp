#include "insert_req_ui.h"
#include<limits>
#include<thread>
#include "miscellaneous.h"
#include "delete_req_ui.h"
#include "select_req_ui.h"
#include "select_by_req_ui.h"
#include "parser_csv.h"
#include <bits/stdc++.h>
#include <unistd.h>


string concat (int, ADRESSE);
string bd_string(int, int, int);

bool cc = true;

void loop_professionnel(){
    while(cc){
        try{
            task_professionnel();
        } catch(...){}
        sleep(10);
    }
}

void loop_particulier(){
    while(cc){
        try{
            task_particulier();

        } catch(...){}
        sleep(10);
    }
}


int id;
int main(){
    string db_str = "../data/dbContacts.db";
    sqlite3* db = open_db(db_str);

    //thread st1(loop_particulier);
    //thread st2(loop_professionnel);
    char choix;

    while (choix!='Q' && choix!='q')
    {
        try{
            cout<< "************************* MENU PRINCIPAL ***************************"
                <<endl << "L - Lister le contenu de la base de donnee"
                <<endl << "R - Rechercher des contacts"
                <<endl << "A - Ajouter un nouveau contact"
                <<endl << "S - Supprimer un contact"
                <<endl << "X - Ajouter un nouveau contact dans .csv"
                <<endl << "Q - Quitter"
                <<endl << "********************************************************************" <<endl;
            cin>>choix;
            switch (choix){
                case('A'):
                case('a'):
                    insert_req_ui(db);
                    break;
                case('S'):
                case('s'):
                    delete_req_ui(db);
                    break;
                case('R'):
                case('r'):
                    select_by_req_ui(db);
                    break;
                case('L'):
                case('l'):
                    select_req_ui(db);
                    break;
                case('Q'):
                case('q'):
                    cout<<"********************************FIN*********************************"<<endl;
                    break;
                default:
                    cout << "choix invalide !!! " << endl;
                    break;
                }
            }
             catch(Exception &ex)
            {
                cout<<endl<<ex.what()<<endl<<endl;
            }
    }
    cc = false;
//    st1.join();
//    st2.join();

    close_db(db);
}

