#include <stdio.h>
#include <stdlib.h>
#include "sqlite3.h"
#include <string.h>
#include <cstring>
#include <iostream>
#include "contact.h"
#include <cstring>
#include "Annuaire.h"
#include "miscellaneous.h"
#include "particulier.h"
#include "professionnel.h"
#include "exception.h"


int get_next_id_req(sqlite3 *db){

    sqlite3_stmt *stmt=NULL;

    const char* requete3 = "SELECT max(IdContact) from CONTACTS;";
    if (sqlite3_prepare_v2(db, requete3, -1, &stmt, NULL) != SQLITE_OK)
    	throw Exception(CauseErreurs::ERR_PREPARE, sqlite3_errmsg(db));
    int identifier = 0;
    while (sqlite3_step(stmt) == SQLITE_ROW)
    {
        identifier = sqlite3_column_int(stmt, 0);
    }

    if(sqlite3_finalize(stmt) != SQLITE_OK)
        throw Exception(CauseErreurs::ERR_FINALIZE, sqlite3_errmsg(db));

    return identifier + 1;
}
