#include "contact.h"
#include "exception.h"
#include <string>
#include <limits>

contact::contact(){
}

contact::contact (int a ,string b,string c, SEXE d)
{
    this->setid(a);
    this->setnom(b);
    this->setprenom(c);
    this->setsexe(d);
}

contact::~contact()
{
}

int contact::getid()
{
    return id;
}

SEXE contact::getsexe()
{
    return sexe;
}

string contact::getnom()
{
    return nom;
}

string contact::getprenom()
{
    return prenom;
}

void contact::setid(int id)
{
    if (to_string(id).length()>INT_ID_SZ || id<=0)
        throw Exception(CauseErreurs::ERR_ID);

    this->id=id;
}

void contact::setsexe(SEXE s)
{
    this->sexe=s;
}

void contact::setnom(string nom)
{
    if (nom.length()>STR_NAME_SZ)
        throw Exception(CauseErreurs::ERR_NAME);

    this->nom = nom;
    for(int i=0; i < (int)nom.length(); i++)
    {
        this->nom[i] = toupper(this->nom[i]);
    }
}

void contact::setprenom(string pren)
{
    if (pren.length()>STR_NAME_SZ)
           throw Exception(CauseErreurs::ERR_NAME);

    this->prenom = pren;
    this->prenom[0] = toupper(prenom[0]);
    for(int i=1; i <STR_NAME_SZ; i++)
    {
        this->prenom[i] = tolower(prenom[i]);
    }
}


ostream& operator<<(ostream& sortie, const ADRESSE& adr)
{

    sortie<<adr.Rue<<" "<<adr.Complt<<" "<<adr.CP<<" "<<adr.Ville<<endl;

    return sortie;

}

void contact::saisie(int id){
    this->setid(id);

    cin.ignore(numeric_limits<streamsize>::max(),'\n');
    cin.clear();
    cout << "Nom: " << endl;
    getline(cin,this->nom);
    cout << "Prenom:" << endl;
    getline(cin,this->prenom);
    cout << "Sexe (M/F)" << endl;
    char _sexe;
    cin >> _sexe ;
    this->setsexe(static_cast<SEXE>(_sexe));
}

