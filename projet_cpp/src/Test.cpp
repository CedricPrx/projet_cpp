#include "Test.h"
#include <stdio.h>
#include <stdlib.h>
#include "sqlite3.h"
#include <string.h>
#include <cstring>
#include <iostream>
#include "contact.h"
#include "miscellaneous.h"
#include "professionnel.h"
#include "particulier.h"
#include "Annuaire.h"
#include "insert_req.h"
#include "select_by_req.h"
#include "delete_req.h"
#include "select_req.h"

using namespace std;


int insert_req_params_test(sqlite3 *db){
	int idContact = 666;
	const char *nom = "Diable";
	const char *prenom = "Mr";
	const char *sexe = "M";
	const char *entreprise = "L'enfer";
	const char *adresse = "15, Volcano St";
	const char *complement = "BozoLand";
	const char *code_postal = "66666";
	const char *ville = "Orlando";
	const char *email = "jesuisfou@hotmail.com";
	const char* date_naissance = "01-01-1988";
	insert_req_params(
		db,
		idContact,
		nom,
		prenom,
		sexe,
		entreprise,
		adresse,
		complement,
		code_postal,
		ville,
		email,
		date_naissance);
}

void test_insert(sqlite3* db){

   	// open a file in read mode.
   	ifstream infile;
   	infile.open("afile.dat");

	std::locale global;
	std::locale loc(global, new _ctype('|'));
    	infile.imbue(loc);

	int idContact;
	char nom[STR_NAME_SZ];
	char prenom[STR_NAME_SZ];
	char sexe;
	char entreprise[STR_NAMENT_SZ];
	char adresse[STR_RUE_SZ];
	char complement[STR_COMPLT_SZ];
	char code_postal[INT_CP_SZ];
	char ville[STR_VILLE_SZ];
	char email[255];
	char date_naissance[10];

   	cout << "Reading from the file" << endl;
	cout << "Nom: " << endl;
	infile >> nom;
	cout << "Prenom:" << endl;
	infile >> prenom;
	cout << "Sexe" << endl;
	infile >> sexe;
	cout << "Entreprise" <<endl;
   	infile >> entreprise;
	cout << "Adresse" << endl;
	infile >> adresse;
	cout << "Complement" << endl;
	infile >> complement;
	cout << "Code Postal" << endl;
	infile >> code_postal;
	cout << "Ville" << endl;
	infile >> ville ;
	cout << "Email" << endl;
	infile >> email;
	cout << "Date Naissance" << endl;
	infile >> date_naissance;
   	cout << "End Reading from the file" << endl;

   	infile.close();
	cout << "Sexe: " << sexe << endl;
//	insert_req_params(
//		db,
//		idContact,
//		nom,
//		prenom,
//		&sexe,
//		entreprise,
//		adresse,
//		complement,
//		code_postal,
//		ville,
//		email,
//		date_naissance);

   	// again read the data from the file and display it.

   	// close the opened file.
}

//
//int main()
//{
//    	string db_path = "../data/dbContacts.db";
//    	sqlite3* db = open_db(db_path);
//    	//select_req(db);
//    	//insert_req_params_test(db);
//	//char *nom = "Diable";
//	//select_by_req(db, NULL, nom);
//	//char *ville = "PARIS";
//	//char *name = "LOUIS";
//	//select_by_req(db, ville, name);
//	//delete_req_params(db, 666);
//	//import_into_base(db);
//
//	test_insert(db);
//    	return 0;
//}



//Fonctions tests --------------------------------------------------------------------------

void test_echange_db()
{
    string db_path = "../data/dbContacts.db";
    sqlite3* db = open_db(db_path);

    cout<<endl<<"Test affichage dbContacts integral"<<endl<<endl;
    select_req(db);

    cout<<endl<<"Test affichage nouveau contact"<<endl<<endl;
    insert_req_params_test(db);
	char *nom = "Diable";
	select_by_req(db, NULL, nom);

	cout<<endl<<"Test affichage contacts par ville et par nom"<<endl<<endl;
	char *ville = "PARIS";
	char *name = "LOUIS";
	select_by_req(db, ville, name);

	delete_req_params(db, 666);
	close_db(db);
}

void test_classes()
{
    cout<<endl<<"Test des classes contacts Pro et Particulier"<<endl<<endl;

    professionnel p1(44,"pinGeot","maZarine",F,"oudpppt@tcom","thalas",{"3, Marat","b",89014,"bures"});

    cout<<p1.affiche()<<endl;

    ADRESSE adress = {"10, rue de la paix", "bal56", 65422, "Lens"};
    string birthd = bd_string(10,12,1986);
    Particulier test(89,"bfkbdf","sssssdv",M, adress, birthd);

    cout<<test.affiche();

    p1.file_export();
    test.file_export();
}

void test_annuaire()
{
    cout<<endl<<"Test de la classe Annuaire"<<endl<<endl;

    ADRESSE adress = {"10, rue de la paix", "bal56", 65422, "Lens"};
    string birthd = bd_string(10,12,1986);

    Annuaire anntest;
    anntest.ajouter(new Particulier(89,"bfkbdf","sssssdv",M, adress, birthd));
    anntest.ajouter(new professionnel(44,"pinGeot","maZarine",F,"oudppt@tcom","thalas",{"3, Marat","b",89014,"bures"}));

    anntest.affiche();
}
