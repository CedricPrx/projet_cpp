#include <bits/stdc++.h>
#include "particulier.h"
#include "professionnel.h"
#include "insert_req.h"
#include "miscellaneous.h"

using namespace std;

void task_professionnel()
{
try
    {

    fstream fin;

    fin.open("../data/newprofs.csv", ios::in);

    string line, word, temp;
    getline(fin, line, '\n');
    while (!fin.eof()) {
        sqlite3 *db = open_db("../data/dbContacts.db");
        getline(fin, line, '\n');
        if (line == ""){
            continue;
        }
        stringstream s(line);
        professionnel prof;

        // Identifiant"
        getline(s, word, ';');

        prof.setid(stoi(word));

        // Nom
        getline(s, word, ';');
        prof.setnom(word);

        //Prénom
        getline(s, word, ';');
        prof.setprenom(word);

        // Sexe
        getline(s, word, ';');
        prof.setsexe(SEXE::F);

        // Entreprise
        getline(s, word, ';');
        prof.setnomEnt(word);

        // Libellé
        ADRESSE adr;
        getline(s, word, ';');
        adr.Rue = word;

        // Complément
        getline(s, word, ';');
        adr.Complt = word;

        // Code Postal
        getline(s, word, ';');
        adr.CP = stoi(word);

        // Ville
        getline(s, word, ';');
        adr.Ville = word;

        prof.setadr(adr);

        // Mail de Contact
        getline(s, word, ';');
        prof.setmail(word);

        char sexe = prof.getsexe() == SEXE::F ? 'F' : 'M';
        insert_req_params(
            db,
            prof.getid(),
            prof.getnom().c_str(),
            prof.getprenom().c_str(),
            &sexe,
            prof.getnomEnt().c_str(),
            prof.getadr().Rue.c_str(),
            prof.getadr().Complt.c_str(),
            to_string(prof.getadr().CP).c_str(),
            prof.getadr().Ville.c_str(),
            prof.getmail().c_str(),
            nullptr);
        close_db(db, false);
    }
    fin.close();

    } catch(...){}
}

void task_particulier()
{
    try
    {
        fstream fin;

        // Open an existing file
        fin.open("../data/newprivates.csv", ios::in);

        string line, word, temp;
        getline(fin, line, '\n');
        while (!fin.eof()) {
            sqlite3 *db = open_db("../data/dbContacts.db");
            getline(fin, line, '\n');
            if (line == ""){
                continue;
            }
            stringstream s(line);

            Particulier part;

            // Identifiant"
            getline(s, word, ';');
            part.setid(stoi(word));

            // Nom
            getline(s, word, ';');
            part.setnom(word);

            //Prénom
            getline(s, word, ';');
            part.setprenom(word);

            // Sexe
            getline(s, word, ';');
            part.setsexe(SEXE::F);

            // Libellé
            ADRESSE adr;
            getline(s, word, ';');
            adr.Rue = word;

            // Complément
            getline(s, word, ';');
            adr.Complt = word;

            // Code Postal
            getline(s, word, ';');
            adr.CP = stoi(word);

            // Ville
            getline(s, word, ';');
            adr.Ville = word;

            part.setadr(adr);

            // Date de Naissance

            getline(s, word, ';');
            part.set_birth_date(word);

            char sexe = part.getsexe() == SEXE::F ? 'F' : 'M';
            insert_req_params(
                    db,
                    part.getid(),
                    part.getnom().c_str(),
                    part.getprenom().c_str(),
                    &sexe,
                    nullptr,
                    part.getadr().Rue.c_str(),
                    part.getadr().Complt.c_str(),
                    to_string(part.getadr().CP).c_str(),
                    part.getadr().Ville.c_str(),
                    nullptr,
                    part.getbirth_date().c_str());
            close_db(db, false);
        }
        fin.close();

    } catch(...){

    }
}

