//
//  professionnel.cpp
//  project_contacts
//
//  Created by Oussama Boukh on 29/12/2022.
//  Copyright © 2022 Oussama Boukh. All rights reserved.
//

#include "professionnel.h"
#include<iomanip>
#include<cstring>
#include "exception.h"
#include <limits>



professionnel::professionnel(){}

professionnel::professionnel(int a,string b,string c, SEXE d,string e,string f,ADRESSE g)
    :contact (a,b,c,d)
{
    this->setmail(e);
    this->setnomEnt(f);
    this->setadr(g);


}

professionnel::~professionnel()
{

    cout<< "destruction du professionnel "<<this->getid()<<endl;

}


string professionnel::getmail()
{

    return mail;

}


string professionnel::getnomEnt()
{

    return nomEnt;

}


ADRESSE professionnel::getadr()
{

    return adr_ent;

}


void professionnel::setmail(string mail)
{

    //char str[] = this->mail;
       // char ch = '@';

       if (mail.find("@") == string::npos)
       throw Exception(CauseErreurs::ERR_MAIL);

    this->mail=mail;


}

void professionnel::setnomEnt(string entr)
{

    if (entr.length()>STR_NAMENT_SZ)
        throw Exception(CauseErreurs::ERR_NAMENT);


    for(int i=0; i <entr.length(); i++)
        {
           entr[i] = toupper(entr[i]);
        }
    this->nomEnt=entr;
}


void professionnel::setadr(ADRESSE ad)
{

    if ( ad.Rue.length() > STR_RUE_SZ || ad.Complt.length() > STR_COMPLT_SZ )
        throw Exception(CauseErreurs::ERR_ADR);

    if (to_string(ad.CP).length()>INT_CP_SZ)
        throw Exception(CauseErreurs::ERR_CP);

    for(int i=0; i <ad.Ville.length(); i++)
             {
                 ad.Ville[i] = toupper(ad.Ville[i]);
             }
    this->adr_ent=ad;

}


string professionnel::affiche()
{

    ostringstream oss;

    oss<<"Professionnel id: "<< setw(5) << setfill('0') <<this->getid()<<endl;
    oss<<"Nom: "<<this->getnom()<<endl;
    oss<<"Prenom: "<<this->getprenom()<<endl;
    oss<<"Sexe: "<<((char)this->getsexe())<<endl;
    oss<<"email: "<<this->getmail()<<endl;
    oss<<"Entreprise: "<<this->getnomEnt()<<endl;
    oss<<"Adresse de l'Entrprise: "<<endl << this->getadr()<<endl;

    return oss.str();
}

void professionnel::file_export()
{
    ofstream File("newprofs.csv", ios::app);
    if (!File)
    {
        throw Exception(CauseErreurs::ERR_FILE);
    }

    File <<this->getid()<<";"
         <<this->getnom()<<";"
         <<this->getprenom()<<";"
         <<(char)this->getsexe()<<";"
         <<this->getnomEnt()<<";"
         <<this->getadr().Rue<<";"
         <<this->getadr().Complt<<";"
         <<this->getadr().CP<<";"
         <<this->getadr().Ville<<";"
         <<this->getmail()<<";"
         <<NULL<<endl;

    File.close();
}



void professionnel::saisie(int id) {
    contact::saisie(id);

    cin.ignore(numeric_limits<streamsize>::max(),'\n');
    cin.clear();
    cout << "Entreprise" <<endl;
       string entreprise;
   getline(cin,entreprise);
    setnomEnt(entreprise);

    cout << "Adresse de l'entreprise" << endl;
    ADRESSE addresse;

    getline(cin,addresse.Rue);
    cout << "Complement" << endl;
    getline(cin,addresse.Complt);
    cout << "Code Postal" << endl;
    cin>>addresse.CP;
    cin.ignore(numeric_limits<streamsize>::max(),'\n');
    cin.clear();
    cout << "Ville" << endl;
    getline(cin,addresse.Ville) ;
    setadr(addresse);

    cout << "Email" << endl;
    string email;
    getline(cin,email);
    setmail(email);

}





