//
//  particulier.cpp
//  project_contacts
//
//  Created by Oussama Boukh on 30/12/2022.
//  Copyright © 2022 Oussama Boukh. All rights reserved.
//

#include "particulier.h"
#include "exception.h"
#include <limits>


Particulier::Particulier(){}

Particulier::Particulier(int id, string n, string p, SEXE s, ADRESSE ad, string birthd )
    :contact(id,n,p,s)
{
    this->setadr(ad);
    this->birth_date = birthd;
}

Particulier::~Particulier()
{
    cout << "Destruction du particulier " << this->getid() << endl;
}
ADRESSE Particulier::getadr()
{
    return adr_part;
}

string Particulier::getbirth_date()
{
    return birth_date;
}

void Particulier::setadr(ADRESSE ad)
{

    if ( ad.Rue.length() > STR_RUE_SZ || ad.Complt.length() > STR_COMPLT_SZ )
        throw Exception(CauseErreurs::ERR_ADR);

    if (to_string(ad.CP).length()>INT_CP_SZ)
        throw Exception(CauseErreurs::ERR_CP);

    for(int i=0; i <ad.Ville.length(); i++)
             {
                 ad.Ville[i] = toupper(ad.Ville[i]);
             }
    this->adr_part=ad;
}


string Particulier::affiche()
{
    ostringstream oss;

    oss
    <<"Particulier id: "<<setw(5)<<setfill('0')<<this->getid()<<endl
    <<"Nom: "<<this->getnom()<<endl
    <<"Prenom: "<<this->getprenom()<<endl
    <<"Sexe: "<<((char)this->getsexe())<<endl
    <<"Date de naissance: "<<this->getbirth_date()<<endl
    <<"Adresse: "<<this->getadr()<<endl;

    return oss.str();

}

void Particulier::file_export()
{
    ofstream File("newprivates.csv", ios::app);
    if (!File)
    {
        throw Exception(CauseErreurs::ERR_FILE);
    }

         File <<this->getid()<<";"
         <<this->getnom()<<";"
         <<this->getprenom()<<";"
         <<(char)this->getsexe()<<";"
         <<NULL<<";"
         <<this->getadr().Rue<<";"
         <<this->getadr().Complt<<";"
         <<this->getadr().CP<<";"
         <<this->getadr().Ville<<";"
         <<NULL<<";"
         <<this->getbirth_date()<<endl;

    File.close();
}

void Particulier::set_birth_date(string val){
    this->birth_date = val;
}

void Particulier::saisie(int id) {
    contact::saisie(id);
    cin.ignore(numeric_limits<streamsize>::max(),'\n');
    cin.clear();
    cout << "Adresse" << endl;
    ADRESSE addresse;
    getline(cin,addresse.Rue);

    cout << "Complement" << endl;
    getline(cin,addresse.Complt);

    cout << "Code Postal" << endl;
    cin >> addresse.CP;

    cin.ignore(numeric_limits<streamsize>::max(),'\n');
    cin.clear();

    cout << "Ville" << endl;
    getline(cin,addresse.Ville);
    setadr(addresse);

    string date_naissance;
    cout << "Date Naissance" << endl;
    getline(cin,date_naissance);
    set_birth_date(date_naissance);
}
