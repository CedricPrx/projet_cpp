#include "sqlite3.h"
#include <iostream>
#include "delete_req.h"

using namespace std;

void delete_req_ui(sqlite3* db){
    int id;
    cout << "Entrer l'identifiant du contact que vous souhaitez supprimer: " << endl;
    cin >> id;
    delete_req_params(db, id);
}
