#include <stdio.h>
#include <stdlib.h>
#include "sqlite3.h"
#include <string.h>
#include <cstring>
#include <iostream>
#include "contact.h"
#include <cstring>
#include "Annuaire.h"
#include "miscellaneous.h"
#include "particulier.h"
#include "professionnel.h"
#include "exception.h"

using namespace std;

int select_req (sqlite3 *db){

    sqlite3_stmt *stmt=NULL;

    char* requete3 = "SELECT * from CONTACTS;";
    if (sqlite3_prepare_v2(db, requete3, -1, &stmt, NULL) != SQLITE_OK)
    	throw Exception(CauseErreurs::ERR_PREPARE, sqlite3_errmsg(db));

    Annuaire liste;
    int i=0;
    while (sqlite3_step(stmt) == SQLITE_ROW)
    {
        int identifier = sqlite3_column_int(stmt, 0);
        string nom = conv_c2s(sqlite3_column_text(stmt, 1));
        string prenom = conv_c2s(sqlite3_column_text(stmt, 2));
        string sexe = conv_c2s(sqlite3_column_text(stmt, 3));
        string entreprise = conv_c2s(sqlite3_column_text(stmt, 4));
        string adresse = conv_c2s(sqlite3_column_text(stmt, 5));
        string complement = conv_c2s(sqlite3_column_text(stmt, 6));
        int cp = sqlite3_column_int(stmt, 7);
        string ville = conv_c2s(sqlite3_column_text(stmt, 8));
        string mail = conv_c2s(sqlite3_column_text(stmt, 9));
        string date = conv_c2s(sqlite3_column_text(stmt, 10));

        i++;
        if (entreprise=="NULL")
            liste.ajouter(new Particulier(identifier,nom,prenom, (SEXE)sexe[0],{adresse, complement, cp, ville}, date));
        else
            liste.ajouter(new professionnel(identifier, nom, prenom, (SEXE)sexe[0], mail, entreprise, {adresse, complement, cp, ville}));
    }

    if(i!=0)
    {
        cout <<endl<<"   " << i << " resultats trouves : " << endl<<endl;
        liste.affiche();
    }
    else{
         cout <<endl<< "    Aucun resultats trouves" << endl<<endl;
    }

    if(sqlite3_finalize(stmt) != SQLITE_OK)
        throw Exception(CauseErreurs::ERR_FINALIZE, sqlite3_errmsg(db));


    cout << "Import base de donnee : SUCCESS" <<endl<<endl;
    return 0;

}
