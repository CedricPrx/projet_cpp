#include <stdio.h>
#include <stdlib.h>
#include "sqlite3.h"
#include <string.h>
#include <cstring>
#include <iostream>
#include "contact.h"
#include <cstring>
#include "exception.h"
using namespace std;

int delete_req_params(sqlite3 *db, int idContact)
{
	char* req = "DELETE FROM CONTACTS where IdContact = ?;";
   	sqlite3_stmt *res=NULL;

    if (sqlite3_prepare_v2(db, req, -1, &res, 0) != SQLITE_OK)
        throw Exception(CauseErreurs::ERR_PREPARE, sqlite3_errmsg(db));

    if (sqlite3_bind_int(res, 1, idContact) != SQLITE_OK)
        throw Exception(CauseErreurs::ERR_BIND, sqlite3_errmsg(db));

  	if (sqlite3_step(res) != SQLITE_DONE)
    	throw Exception(CauseErreurs::ERR_EXEC, sqlite3_errmsg(db));

    if (sqlite3_finalize(res) != SQLITE_OK)
    	throw Exception(CauseErreurs::ERR_FINALIZE, sqlite3_errmsg(db));

	cout<<endl<<"Delete : SUCCESS"<<endl<<endl;
	return 0;
}
