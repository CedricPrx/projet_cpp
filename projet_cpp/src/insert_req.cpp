#include <stdio.h>
#include <stdlib.h>
#include "sqlite3.h"
#include <string.h>
#include <cstring>
#include <iostream>
#include "contact.h"
#include <cstring>
#include "exception.h"

using namespace std;


void sql_bind_text(sqlite3 *db, int& rc, sqlite3_stmt *res, const char* value, int pos){
    if (rc == SQLITE_OK) {
        if (value!= nullptr){
            rc = sqlite3_bind_text(res, pos, value, strlen(value), NULL);
        } else {
            sqlite3_bind_null(res, pos);
        }
    } else {
        throw Exception(CauseErreurs::ERR_BIND, sqlite3_errmsg(db));
    }
}

int insert_req_params(
		sqlite3 *db,
		int idContact,
		const char *nom,
		const char *prenom,
		const char *sexe,
		const char *entreprise,
		const char *adresse,
		const char *complement,
		const char *code_postal,
		const char *ville,
		const char *email,
		const char *date_naissance)
{

    int rc=0;
	char* req = "INSERT INTO CONTACTS VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?  )";

   	sqlite3_stmt *res;

    if (sqlite3_prepare_v2(db, req, -1, &res, 0) != SQLITE_OK)
        throw Exception(CauseErreurs::ERR_PREPARE, sqlite3_errmsg(db));

    if (sqlite3_bind_int(res, 1, idContact) != SQLITE_OK)
        throw Exception(CauseErreurs::ERR_BIND, sqlite3_errmsg(db));

    sql_bind_text(db, rc, res, nom, 2);
    sql_bind_text(db, rc, res, prenom, 3);
    sql_bind_text(db, rc, res, sexe, 4);
    sql_bind_text(db, rc, res, entreprise, 5);
    sql_bind_text(db, rc, res, adresse, 6);
    sql_bind_text(db, rc, res, complement, 7);
    sql_bind_text(db, rc, res, code_postal, 8);
    sql_bind_text(db, rc, res, ville, 9);
    sql_bind_text(db, rc, res, email, 10);
    sql_bind_text(db, rc, res, date_naissance, 11);


  	if (rc == SQLITE_OK) {
        if (sqlite3_step(res) != SQLITE_DONE)
            throw Exception(CauseErreurs::ERR_EXEC, sqlite3_errmsg(db));
  	}

    if (sqlite3_finalize(res) != SQLITE_OK)
    	throw Exception(CauseErreurs::ERR_FINALIZE, sqlite3_errmsg(db));

	cout <<"Insertion contact : SUCCESS" <<endl<<endl;
	return rc;
}
