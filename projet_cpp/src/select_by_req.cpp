#include <stdio.h>
#include <stdlib.h>
#include "sqlite3.h"
#include <string.h>
#include <cstring>
#include <iostream>
#include "contact.h"
#include <cstring>
#include "Annuaire.h"
#include "particulier.h"
#include "professionnel.h"
#include "miscellaneous.h"
#include "exception.h"

using namespace std;


int select_by_req(sqlite3 *db, const char *ville, const char *name){

    sqlite3_stmt *stmt = NULL;
    const char *req = "select * from CONTACTS where Ville LIKE ? AND Nom LIKE ?;";
    const char *req2 = "select * from CONTACTS where Ville LIKE ?;";
    const char *req3 = "select * from CONTACTS where Nom LIKE ?;";
    const char *req4 = "select * from CONTACTS;";

    const char *chosenReq = NULL;
    bool nameEmpty = name == NULL || strlen(name) == 0;
	bool villeEmpty = ville == NULL || strlen(ville) == 0;
	if ( !villeEmpty and !nameEmpty ){
		chosenReq = req;
	} else if ( !villeEmpty){
		chosenReq = req2;
	} else if ( !nameEmpty){
		chosenReq = req3;
	} else {
		chosenReq = req4;
	}
	cout << "Chosen Req " << chosenReq << endl;

    if (sqlite3_prepare_v2(db, chosenReq, -1, &stmt, NULL) != SQLITE_OK)
        throw Exception(CauseErreurs::ERR_PREPARE, sqlite3_errmsg(db));

    if (!villeEmpty) {
        if (sqlite3_bind_text(stmt, 1, ville, strlen(ville), NULL) != SQLITE_OK)
            throw Exception(CauseErreurs::ERR_BIND, sqlite3_errmsg(db));
        //cout << "Bind ville " << ville << endl<<endl;
    }

    if (!nameEmpty) {
        if (sqlite3_bind_text(stmt, (villeEmpty ? 1 : 2), name, strlen(name), NULL) != SQLITE_OK)
            throw Exception(CauseErreurs::ERR_BIND, sqlite3_errmsg(db));
        //cout << "Bind nom " << name << endl<<endl;
    }

    Annuaire liste;
    int i=0;
    while (sqlite3_step(stmt) == SQLITE_ROW)
    {
        int identifier = sqlite3_column_int(stmt, 0);
        string nom =  conv_c2s(sqlite3_column_text(stmt, 1));
        string prenom = conv_c2s(sqlite3_column_text(stmt, 2));
        string sexe = conv_c2s(sqlite3_column_text(stmt, 3));
        string entreprise = conv_c2s(sqlite3_column_text(stmt, 4));
        string adresse = conv_c2s(sqlite3_column_text(stmt, 5));
        string complement = conv_c2s(sqlite3_column_text(stmt, 6));
        int cp = sqlite3_column_int(stmt, 7);
        string ville = conv_c2s(sqlite3_column_text(stmt, 8));
        string mail = conv_c2s(sqlite3_column_text(stmt, 9));
        string date = conv_c2s(sqlite3_column_text(stmt, 10));

        i++;
        if (entreprise=="NULL")
            liste.ajouter(new Particulier(identifier,nom,prenom, (SEXE)sexe[0],{adresse, complement, cp, ville}, date));
        else
            liste.ajouter(new professionnel(identifier, nom, prenom, (SEXE)sexe[0], mail, entreprise, {adresse, complement, cp, ville}));
    }
    if(i!=0)
    {
        cout <<endl<<"   " << i << " resultats trouves ! " << endl<<endl;
        liste.affiche();
    }
    else{
         cout <<endl<< "    Aucun resultats trouves" << endl<<endl;
    }

    liste.affiche();

    if (int rc = sqlite3_finalize(stmt) != SQLITE_OK)
        throw Exception(CauseErreurs::ERR_FINALIZE, sqlite3_errmsg(db));

    cout << "Selection par parametre de recherche : SUCCESS" <<endl<<endl;
    return 0;
}
