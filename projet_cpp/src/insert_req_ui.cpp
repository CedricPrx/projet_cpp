#include <string>
#include "insert_req_ui.h"
#include "particulier.h"
#include "professionnel.h"
#include "contact.h"
#include "insert_req.h"
#include "get_next_id_req.h"

void insert_req_ui(sqlite3* db){

   	// close the opened file.
    int choice;

    cout << "Voulez-vous inserer un contact particulier (1) ou professionnel (2)? (1|2) " << endl;
    cin >> choice;
    int id = get_next_id_req(db);

    switch (choice)
    {
        case 1:
        {
            Particulier part;
            part.saisie(id);
            char sexe = part.getsexe() == SEXE::F ? 'F' : 'M';
            insert_req_params(
                db,
                part.getid(),
                part.getnom().c_str(),
                part.getprenom().c_str(),
                &sexe,
                nullptr,
                part.getadr().Rue.c_str(),
                part.getadr().Complt.c_str(),
                to_string(part.getadr().CP).c_str(),
                part.getadr().Ville.c_str(),
                nullptr,
                part.getbirth_date().c_str());
                break;
        }
        case 2:
        {
            professionnel pro;
            pro.saisie(id);
            char sexe = pro.getsexe() == SEXE::F ? 'F' : 'M';
            insert_req_params(
                db,
                pro.getid(),
                pro.getnom().c_str(),
                pro.getprenom().c_str(),
                &sexe,
                pro.getnomEnt().c_str(),
                pro.getadr().Rue.c_str(),
                pro.getadr().Complt.c_str(),
                to_string(pro.getadr().CP).c_str(),
                pro.getadr().Ville.c_str(),
                pro.getmail().c_str(),
                nullptr);

        }
            break;
        default:
            cout << "choix invalide" << endl;
    }
}
