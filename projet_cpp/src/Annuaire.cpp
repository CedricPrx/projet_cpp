#include "Annuaire.h"

/*
Classe stockant dynamiquement dans un vecteur les differents contacts
ajoutÚs par une requete sur la base de donnees.
Contient une methode affiche pour afficher l'integralitÚ des contacts
dans un format lisible en ligne de commande.
*/

Annuaire::~Annuaire()
{
    for(auto element_list : liste_contact)
        delete element_list;
}

void Annuaire::ajouter(contact* cont)
{
    liste_contact.push_back(cont);
}

void Annuaire::affiche()
{
    for(auto contact : liste_contact)
        cout<< contact->affiche()
            <<"-------------------------------------------------"<<endl<<endl;

}

