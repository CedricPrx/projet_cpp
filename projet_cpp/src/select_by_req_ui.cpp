#include <iostream>
#include "sqlite3.h"
#include <limits>
#include "contact.h"
#include "select_by_req.h"

using namespace std;
string msg_ville = "Ecrire la ville pour laquelle vous voulez faire la recherche (si vous ne souhaitez pas rechercher a partir d une ville taper entree)";
string msg_nom = "Ecrire le nom recherche (si vous ne souhaitez pas rechercher de nom taper entree)";

void select_by_req_ui(sqlite3* db){

        cout << "Que voulez-vous faire? "<< endl;
        cout << "(1) Recherche par nom et par ville"<<endl;
        cout << "(2) Recherche par ville"<<endl;
        cout << "(3) Recherche par nom"<<endl;
        char choice;
        cin >> choice;
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
        cin.clear();

        ADRESSE ad;
        ad.Ville="";
        string nom= "";

        switch(choice){
            case '1':
                cout << msg_ville << endl;
                cin >> ad.Ville;
                cout << msg_nom << endl;
                cin >> nom;
                break;
            case '2':
                cout << msg_ville << endl;
                cin >> ad.Ville;
                break;
            case '3':
                cout << msg_nom << endl;
                cin >> nom;
                break;
            default:
                cout << "Choix invalide" << endl;
                return;
        }
        select_by_req(db, ad.Ville.c_str(), nom.c_str());
    }


