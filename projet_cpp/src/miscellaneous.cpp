#include "miscellaneous.h"
#include <stdio.h>
#include <stdlib.h>
#include "sqlite3.h"
#include <string.h>
#include <cstring>
#include <iostream>
#include "contact.h"
#include <cstring>
#include "exception.h"
#include <sstream>

using namespace std;

sqlite3* open_db(string db_path){

    sqlite3 *db=NULL;        // connection Base

    if (sqlite3_open(db_path.c_str(), &db) != SQLITE_OK)
        throw Exception(CauseErreurs::ERR_OPEN, sqlite3_errmsg(db));

    cout << "Base ouverte avec succes." << endl<<endl;
    return db;
}

void close_db(sqlite3* db, bool throwit){

    if (sqlite3_close(db) != SQLITE_OK && throwit)
        throw Exception(CauseErreurs::ERR_CLOSE, sqlite3_errmsg(db));

    cout << "Base fermee avec succes." << endl<<endl;
}

int insert_req (sqlite3 *db)
{
    char *err_msg=NULL;       // pointeur vers err
	char* req = "INSERT INTO CONTACTS VALUES(666, \"Diable\", \"Mr\", \'M\', \"L'enfer\", \"15, Volcano St\", NULL, 66666, \"Orlando\", \"jesuisfou@hotmail.com\", NULL  )";
	if (sqlite3_exec(db, req, 0, 0, &err_msg)!= SQLITE_OK)
		throw Exception(CauseErreurs::ERR_EXEC, sqlite3_errmsg(db));

	return 0;
}


//Fonctions de conversion----------------------------------------------------------------------------------------------

string bd_string(int J, int M, int A)
{
    ostringstream oss ;
    if ( J>0 && J<=31 && M>0 && M<=12 )
        oss << J << "/" << M << "/" << A;
    else throw Exception(CauseErreurs::ERR_BD);

    return oss.str();
}

string conv_c2s(const unsigned char* val)
{
    string a= "NULL" ;
    val == NULL ?  a : a=(const char*)val;
    return a;
}


