//
//  insert_req_ui.cpp
//  test_co2
//
//  Created by Oussama Boukh on 31/12/2022.
//  Copyright © 2022 Oussama Boukh. All rights reserved.
//

#include "insert_req_ui.h"

string concat (int, ADRESSE);
string bd_string(int, int, int);
int newid (int,int);

string poup;
int id;
string nom;
string prenom;
char s;
int num;
char com;
SEXE sexe;
string nent;
string mail;
ADRESSE ad;
string dte;
string ville;
int jour;
int mois;
int an;
int m=0;


void insert_req_ui (sqlite3* db)
{

while (m==0){
   
    cout<<" Ecrire A pour ajouter un contact, S pour supprimer un contact, R pour rechercher un contact, L pour lister in contact ou Q pour quitter."<<endl;
    cin>>com;
    
    switch (com)
    {
        case('A'):
        case('a'):
           
           
            cout<<"Entrer le type de contact (pro pour professionnel ou part pour particulier):"<<"\n";
            cin>>poup;
            cout<<"\n";
    
        
            if(poup=="pro")
                {
                    cout<<"\n";
                    cout<<"Nom: "<<"\n";
                    getline(cin,nom,'\n');
                    
                    
                    cout<<endl<<"Prenom: "<<"\n";
                    getline(cin,prenom,'\n');
                    
                    cout<<"Sexe: "<<endl;
                    cin>>s;
                    sexe=static_cast<SEXE>(s);
                    
                    cout<<"Nom de l'entreprise: "<<endl;
                    cin>>nent;
                    getline(cin,nent,'\n');
                    
                    cout<<"Numero de l'adresse de l'entreprise: "<<endl;
                    cin>>num;
                    
                    cout<<"Nom de la rue de l'entreprise:"<<endl;
                    cin>>ad.Rue;
                    getline(cin,ad.Rue,'\n');
                    
                    cout<<"Complément d'adresse de l'entreprise: "<<endl;
                    cin>>ad.Complt;
                    getline(cin,ad.Complt,'\n');
                    
                    cout<<"Code postal: "<<endl;
                    cin>>ad.CP;
                    
                    cout<<"Ville: "<<endl;
                    cin>>ad.Ville;
                    getline(cin,ad.Ville,'\n');
                    
                    cout<<"email: "<<endl;
                    cin>>mail;
                    
                    id=newid(num,12);
                    
                    ad.Rue=concat(num,ad);
                    
                    professionnel pro(id,nom,prenom,sexe,mail,nent,ad);
                    cout<<pro.affiche();
                    
                   insert_req_params(
                    db,
                    id,
                    nom,
                    prenom,
                    sexe,
                    nent,
                    ad.Rue,
                    ad.Complt,
                    ad.CP,
                    ad.Ville,
                    mail,
                    NULL);
                    
                    
                    pro.file_export();
                }
            else if(poup=="part")
                {
            
                    cout<<"Nom: "<<"\n"<<endl;
                    cin>>nom;
                    getline(cin,nom);
                    
                    cout<<"Prenom: "<<endl;
                    cin>>prenom;
                    getline(cin,prenom);
                    
                    cout<<"Sexe: "<<endl;
                    cin>>s;
                    sexe=static_cast<SEXE>(s);
                    
                    cout<<"Numero de l'adresse du contact: "<<endl;
                    cin>>num;
                    
                    cout<<"Nom de la rue du contact:"<<endl;
                    cin>>ad.Rue;
                    getline(cin,ad.Rue);
                    
                    cout<<"Complément d'adresse du contact: "<<endl;
                    cin>>ad.Complt;
                    getline(cin,ad.Complt);
                    
                    cout<<"Code postal: "<<endl;
                    cin>>ad.CP;
                    
                    cout<<"Ville: "<<endl;
                    cin>>ad.Ville;
                    getline(cin,ad.Ville);
                    
                    cout<<"Jour de naissance: "<<endl;
                    cin>>jour;
                    cout<<"Mois de naissance: "<<endl;
                    cin>>mois;
                    cout<<"Année de naissance: "<<endl;
                    cin>>an;
                    
                    ad.Rue=concat(num,ad);
                    id=newid(num,12);
                    
                    dte=bd_string(jour,mois,an);
                    
                    Particulier part(id,nom,prenom,sexe,ad,dte);
                    
                    cout<<part.affiche();
                    
                    part.file_export();
                    
                  
            
            
                }
            else
                {
                    break;
                }

            break;
        case ('S'):
        case ('s'):
            
            cout<< "Entrer l'identifiant du contact que vous souhaitez supprimer: "<<endl;
            cin>>id;
            
            //delete_req_params(sqlite3 *db,id);
            break;
            
            
        case ('R'):
        case ('r'):
            
            
            
            cout<<"Ecrire le nom recherché (si vous ne souhaité pas rechercher de nom taper entrée): "<<endl;
            cin>>nom;
            cout<<"Ecrire la ville pour laquelle vous voulez faire la recherche (si vous ne souhaité pas rechercher à partir d'une ville taper entrée): "<<endl;
            cin>>ville;
            
            //select_by_req(sqlite3 *db, ville, nom);
            break;
        
        case ('Q'):
        case ('q'):
            cout<<"vous avez stoppé l'interface de manière imprévue!!"<<endl;
            break;
        default:
            cout<<"choix invalide!!!"<<endl;
            break;
            
    }
    
    }
   
}
