//
//  main.cpp
//  project_contacts
//
//  Created by Oussama Boukh on 29/12/2022.
//  Copyright © 2022 Oussama Boukh. All rights reserved.
//

#include <iostream>
#include "contact.h"
#include "exception.h"
#include "professionnel.h"
#include "particulier.h"
#include "Annuaire.h"
#include "../sqlite/sqlite3.h"
#include <cstring>


//Prototypes fonctions de conversion
string bd_string(int, int, int);
string concat (int, ADRESSE );
extern string conv_c2s(const unsigned char*);

//Prototypes fonctions test Hierarchie classes
void classes_test();
void annuaire_test();

//Prototype fonctions sqlite3

sqlite3* open_db(string);
int insert_req (sqlite3*);
int delete_req_params(sqlite3*, int);
int insert_req_params(sqlite3*,int,char*,char*,char*,char*,char*,char*,char*,char*,char*,char*);
int select_req (sqlite3*);
int select_by_req(sqlite3*, const char*, const char*);

//Prototypes test sqlite3
int insert_req_params_test(sqlite3*);

//Main----------------------------------------------------------------------------------------------------

int main() {



    try{
       classes_test();
       cout<<endl;
       annuaire_test();
    }

    catch(const Exception &ex)
    {
        cout<<ex.getCause()<<endl;
    }
    catch(...)
    {
        cout<<"Autre erreur"<<endl;
    }



 /*   string db_path = "../data/dbContacts.db";
    sqlite3* db = open_db(db_path);
    //select_req(db);
    //insert_req_params_test(db);
	char *nom = "Diable";
	select_by_req(db, NULL, nom);
	char *ville = "PARIS";
	char *name = "LOUIS";
	select_by_req(db, ville, name);
	//delete_req_params(db, 666);
*/


    return 0;
}

//Fonctions de conversion----------------------------------------------------------------------------------------------

string bd_string(int J, int M, int A)
{
    ostringstream oss ;
    if ( J>0 && J<=31 && M>0 && M<=12 )
        oss << J << "/" << M << "/" << A;
    else throw Exception(CauseErreurs::ERR_BD);

    return oss.str();
}

string concat (int n, ADRESSE a)
{
    ostringstream oss;
    oss<<n<<", "<<a.Rue;

    return oss.str();
}


//Fonctions test --------------------------------------------------------------------------
void classes_test()
{
        professionnel p1(44,"pinGeot","maZarine",F,"oudpppt@tcom","thalas",{"3, Marat","b",89014,"bures"});

        cout<<p1.affiche()<<endl;

        ADRESSE adress = {"10, rue de la paix", "bal56", 65422, "Lens"};
        string birthd = bd_string(10,12,1986);
        Particulier test(89,"bfkbdf","sssssdv",M, adress, birthd);

        cout<<test.affiche();

        p1.file_export();
        test.file_export();
}

void annuaire_test()
{
    ADRESSE adress = {"10, rue de la paix", "bal56", 65422, "Lens"};
    string birthd = bd_string(10,12,1986);

    Annuaire anntest;
    anntest.ajouter(new Particulier(89,"bfkbdf","sssssdv",M, adress, birthd));
    anntest.ajouter(new professionnel(44,"pinGeot","maZarine",F,"oudppt@tcom","thalas",{"3, Marat","b",89014,"bures"}));

    anntest.affiche();
}




