//
//  particulier.hpp
//  project_contacts
//
//  Created by Oussama Boukh on 30/12/2022.
//  Copyright © 2022 Oussama Boukh. All rights reserved.
//

#ifndef particulier_hpp
#define particulier_hpp

#include <stdio.h>
#include <iostream>
#include <string>
#include "contact.h"
#include <sstream>



class Particulier : public  contact
{
    public:
        Particulier(int, string, string, SEXE, ADRESSE, string);
        ~Particulier();

        //getters / setters
        ADRESSE getadr();
        string getbirth_date();

        void setadr(ADRESSE);

        string affiche();
        void file_export();


    protected:

    private:
        ADRESSE adr_part;
        string birth_date;

};



#endif // PARTICULIER_HPP



