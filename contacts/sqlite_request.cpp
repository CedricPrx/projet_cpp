#include <stdio.h>
#include <stdlib.h>
#include "../sqlite/sqlite3.h"
#include <string.h>
#include <cstring>
#include <iostream>
#include "../contacts/Annuaire.h"
#include "../contacts/particulier.h"
#include "../contacts/professionnel.h"

using namespace std;

string conv_c2s(const unsigned char* val)
{
    string a= "NULL" ;
    val == NULL ?  a : a=(const char*)val;
    return a;
}




sqlite3* open_db(string db_path ){

    sqlite3 *db=NULL;        // connection Base

    int rc = sqlite3_open(db_path.c_str(), &db);
    if (rc != SQLITE_OK) {
        cout << "ERREUR Open : " << sqlite3_errmsg(db) << endl;
       return NULL;
    }

    cout << "Base ouverte avec succes." << endl;
    return db;
}

int insert_req (sqlite3 *db)
{
    char *err_msg=NULL;       // pointeur vers err
	char* req = "INSERT INTO CONTACTS VALUES(666, \"Diable\", \"Mr\", \'M\', \"L'enfer\", \"15, Volcano St\", NULL, 66666, \"Orlando\", \"jesuisfou@hotmail.com\", NULL  )";
	int rc = sqlite3_exec(db, req, 0, 0, &err_msg);
	if (rc != SQLITE_OK){
		cout << "Erreur lors de l'ecriture:" << sqlite3_errmsg(db) << endl;
	}
	return rc;
}

int delete_req_params(sqlite3 *db, int idContact)
{

    char *err_msg=NULL;       // pointeur vers err
	char* req = "DELETE FROM CONTACTS where IdContact = ?;";
   	sqlite3_stmt *res;
    	int rc = sqlite3_prepare_v2(db, req, -1, &res, 0);

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_int(res, 1, idContact);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

	if (rc != SQLITE_OK){
		cout << "Failed to bind " << sqlite3_errmsg(db) << endl;
	}
	// TODO: check why error code incorrect
	//
  	if (sqlite3_step(res) != SQLITE_OK) {
    		printf("\nCould not step (execute) stmt.\n");
    		return 1;
  	}

    	sqlite3_finalize(res);

	if (rc != SQLITE_OK){
		cout << "Erreur lors de l'ecriture:" << sqlite3_errmsg(db) << endl;
	}
	return rc;
}

int insert_req_params(
		sqlite3 *db,
		int idContact,
		char *nom,
		char *prenom,
		char *sexe,
		char *entreprise,
		char *adresse,
		char *complement,
		int code_postal,
		char *ville,
		char *email,
		char *date_naissance)
{

    	char *err_msg=NULL;       // pointeur vers err
	char* req = "INSERT INTO CONTACTS VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?  )";

   	sqlite3_stmt *res;
    	int rc = sqlite3_prepare_v2(db, req, -1, &res, 0);

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_int(res, 1, idContact);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_text(res, 2, nom, strlen(nom), NULL);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_text(res, 3, prenom, strlen(prenom), NULL);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_text(res, 4, sexe, strlen(sexe), NULL);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_text(res, 5, entreprise, strlen(entreprise), NULL);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_text(res, 6, adresse, strlen(adresse), NULL);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_text(res, 7, complement, strlen(complement), NULL);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_int(res, 8, code_postal);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_text(res, 9, ville, strlen(ville), NULL);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_text(res, 10, email, strlen(email), NULL);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_text(res, 11, date_naissance, strlen(date_naissance), NULL);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

        if (rc != SQLITE_OK){
            cout << "Failed to bind " << sqlite3_errmsg(db) << endl;
        }


  	if (sqlite3_step(res) != SQLITE_DONE) {
    		printf("\nCould not step (execute) stmt.\n");
    		return 1;
  	}

    	sqlite3_finalize(res);

	if (rc != SQLITE_OK){
		cout << "Erreur lors de l'ecriture:" << sqlite3_errmsg(db) << endl;
	}
	return rc;
}

int insert_req_params_test(sqlite3 *db){
	int idContact = 666;
	char *nom = "Diable";
	char *prenom = "Mr";
	char *sexe = "M";
	char *entreprise = "L'enfer";
	char *adresse = "15, Volcano St";
	char *complement = "BozoLand";
	int code_postal = 66666;
	char *ville = "Orlando";
	char *email = "jesuisfou@hotmail.com";
	char* date_naissance = "01-01-1988";
	insert_req_params(
		db,
		idContact,
		nom,
		prenom,
		sexe,
		entreprise,
		adresse,
		complement,
		code_postal,
		ville,
		email,
		date_naissance);
}

int select_req (sqlite3 *db){

    char *errmsg=NULL;       // pointeur vers err
    sqlite3_stmt *stmt=NULL;

    char* requete3 = "SELECT * from CONTACTS LIMIT 10;";
    if (sqlite3_prepare_v2(db, requete3, -1, &stmt, NULL)){
    	cout << "Dommage! " << endl;
    }

    int rc;             // code retour

    Annuaire liste;

    while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {
	int identifier = sqlite3_column_int(stmt, 0);
	string nom =  conv_c2s(sqlite3_column_text(stmt, 1));
	string prenom = conv_c2s(sqlite3_column_text(stmt, 2));
	const char* sexe = (const char*)sqlite3_column_text(stmt, 3);
	string entreprise = conv_c2s(sqlite3_column_text(stmt, 4));
	string adresse = conv_c2s(sqlite3_column_text(stmt, 5));
	string complement = conv_c2s(sqlite3_column_text(stmt, 6));
	int cp = sqlite3_column_int(stmt, 7);
	string ville = conv_c2s(sqlite3_column_text(stmt, 8));
	string mail = conv_c2s(sqlite3_column_text(stmt, 9));
	string date = conv_c2s(sqlite3_column_text(stmt, 10));


	if (entreprise=="NULL")
        liste.ajouter(new Particulier(identifier,nom,prenom, (SEXE)sexe[0],{adresse, complement, cp, ville}, date));
    else
        liste.ajouter(new professionnel(identifier, nom, prenom, (SEXE)sexe[0], mail, entreprise, {adresse, complement, cp, ville}));

/*
	cout<< "Identifier: " << identifier << " ; "
		<< "Nom: " << (nom == NULL ? "NULL" : nom << " ; "
        << "Prenom: " << (prenom == NULL ? "NULL" : prenom) << " ; "
		<< "Sexe: " << (sexe == NULL ? "NULL" : sexe) << " ; "
        << "Entreprise: " << ( entreprise == NULL ? "NULL" : entreprise )<< " ; "
        << "Adresse: " << ( adresse == NULL ? "NULL" : adresse )<< " ; "
		<< "Complement: " << (complement == NULL ? "NULL" : complement ) << " ; "
		<< "Code Postal: " << cp  << " ; "
		<< " Ville: " << (ville == NULL ? "NULL" : ville )<< " ; "
		<< "Mail" << ( mail == NULL ? "NULL" : mail ) << " ; "
		<< "Date: " << (date == NULL ? "NULL" : date)  << " ; "
		<< endl;
*/
    }

    liste.affiche();

    sqlite3_finalize(stmt);
    rc = sqlite3_close(db);

    if (rc != SQLITE_OK) {
        printf("ERREUR Open : %s\n", sqlite3_errmsg(db));
       return 1;
    }
    return 0;

}


int select_by_req(sqlite3 *db, const char *ville, const char *name){

    	char *errmsg=NULL;       // pointeur vers err
    	sqlite3_stmt *stmt=NULL;
    	char *req = "select * from CONTACTS where Ville LIKE ? AND Nom LIKE ?;";
    	char *req2 = "select * from CONTACTS where Ville LIKE ?;";
    	char *req3 = "select * from CONTACTS where Nom LIKE ?;";
    	char *req4 = "select * from CONTACTS;";

        char *chosenReq = NULL;
    	int rc;             // code retour

	if (ville != NULL and name != NULL){
		chosenReq = req;
	} else if ( ville != NULL){
		chosenReq = req2;
	} else if ( name != NULL){
		chosenReq = req3;
	} else {
		chosenReq = req4;
	}
	cout << "Chosen Req" << chosenReq << endl;

    	if ((rc = sqlite3_prepare_v2(db, chosenReq, -1, &stmt, NULL)) != SQLITE_OK){
    		cout << "Dommage! " << endl;
    	}

    	if (rc == SQLITE_OK && ville != NULL) {
        	rc = sqlite3_bind_text(stmt, 1, ville, strlen(ville), NULL);
		cout << "Bind ville " << ville << endl;
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	if (rc == SQLITE_OK && name != NULL) {
        	rc = sqlite3_bind_text(stmt, (ville == NULL ? 1 : 2), name, strlen(name), NULL);
		cout << "Bind nom " << name << endl;
    	} else {
        	fprintf(stderr, "Failed to bind parameter: %s\n", sqlite3_errmsg(db));
    	}

    	Annuaire liste;

    	while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW)
    	{
		int identifier = sqlite3_column_int(stmt, 0);
		string nom =  conv_c2s(sqlite3_column_text(stmt, 1));
		string prenom = conv_c2s(sqlite3_column_text(stmt, 2));
		string sexe = conv_c2s(sqlite3_column_text(stmt, 3));
		string entreprise = conv_c2s(sqlite3_column_text(stmt, 4));
        string adresse = conv_c2s(sqlite3_column_text(stmt, 5));
        string complement = conv_c2s(sqlite3_column_text(stmt, 6));
        int cp = sqlite3_column_int(stmt, 7);
		string ville = conv_c2s(sqlite3_column_text(stmt, 8));
		string mail = conv_c2s(sqlite3_column_text(stmt, 9));
		string date = conv_c2s(sqlite3_column_text(stmt, 10));

		if (entreprise=="NULL")
            liste.ajouter(new Particulier(identifier,nom,prenom, (SEXE)sexe[0],{adresse, complement, cp, ville}, date));
        else
            liste.ajouter(new professionnel(identifier, nom, prenom, (SEXE)sexe[0], mail, entreprise, {adresse, complement, cp, ville}));

/*
		cout<< "Identifier: " << identifier << " ; "
			<< "Nom: " << (nom == NULL ? "NULL" : nom)<< " ; "
            << "Prenom: " << (prenom == NULL ? "NULL" : prenom) << " ; "
			<< "Sexe: " << (sexe == NULL ? "NULL" : sexe) << " ; "
            << "Entreprise: " << ( entreprise == NULL ? "NULL" : entreprise )<< " ; "
            << "Adresse: " << ( adresse == NULL ? "NULL" : adresse )<< " ; "
			<< "Complement: " << (complement == NULL ? "NULL" : complement ) << " ; "
			<< "Code Postal: " << cp << " ; "
			<< " Ville: " << (ville == NULL ? "NULL" : ville )<< " ; "
			<< "Mail" << ( mail == NULL ? "NULL" : mail ) << " ; "
			<< "Date: " << (date == NULL ? "NULL" : date)  << " ; "
			<< endl;
*/
    	}

        liste.affiche();

    	sqlite3_finalize(stmt);
    	rc = sqlite3_close(db);

    	if (rc != SQLITE_OK) {
        	printf("ERREUR Open : %s\n", sqlite3_errmsg(db));
       		return 1;
    	}
    	return 0;
}

