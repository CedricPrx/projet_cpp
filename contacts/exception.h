#ifndef exception_hpp
#define exception_hpp
#include <iostream>
#include <exception>
#include <string>

using namespace std;

enum CauseErreurs {ERR_ID, ERR_NAME, ERR_ADR, ERR_CP, ERR_BD, ERR_NAMENT, ERR_MAIL, ERR_FILE};

class Exception: public exception
{
    public:
        Exception(CauseErreurs errCause)
        {
            switch(errCause)
            {
                 case CauseErreurs::ERR_ID:
                    this->cause = "Erreur : entrer identifiant non nul et limite a 5 chiffres positifs.";
                    break;

                case CauseErreurs::ERR_NAME:
                    this->cause = "Erreur : entrer nom / prenom limite a 30 caracteres.";
                    break;

                case CauseErreurs::ERR_ADR:
                    this->cause = "Erreur : entrer adresse / complement d'adresse limite a 250 caracteres.";
                    break;

                case CauseErreurs::ERR_CP:
                    this->cause = "Erreur : entrer code postal limite a 5 chiffres.";
                    break;

                case CauseErreurs::ERR_BD:
                    this->cause = "Erreur : date de naissance invalide.";
                    break;
                
                case CauseErreurs::ERR_NAMENT:
                    this->cause = "Erreur : entrer nom de l'entreprise / limite a 50 caracteres.";
                    break;
                    
                case CauseErreurs::ERR_MAIL:
                    this->cause = "Erreur : adresse mail invalide.";
                    break;
                    
                case CauseErreurs::ERR_FILE:
                    this->cause = "Erreur : pb fichier csv";
                    break;
                    
                default:
                    this->cause = "Autre Erreur";
                    break;
            }
        }

        string getCause() const noexcept
        {
            return this->cause;
        }

    private:
        string cause;
};


#endif /* exception_hpp */

