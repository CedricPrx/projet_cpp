#include "Annuaire.h"


Annuaire::~Annuaire()
{
    for(auto element_list : liste_contact)
        delete element_list;
}

void Annuaire::ajouter(contact* cont)
{
    liste_contact.push_back(cont);
}

void Annuaire::affiche()
{
    for(auto contact : liste_contact)
        cout << contact->affiche();
}

