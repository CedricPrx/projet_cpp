/*#include <stdio.h>
#include <stdlib.h>
#include "sqlite3.h"
#include <string.h>
#include <cstring>
#include <iostream>
#include "../contacts/contact.hpp"
using namespace std;

sqlite3* open_db(string db_path){

    sqlite3 *db=NULL;        // connection Base

    int rc = sqlite3_open(db_path.c_str(), &db);
    if (rc != SQLITE_OK) {
        cout << "ERREUR Open : " << sqlite3_errmsg(db) << endl;
       return NULL;
    }

    cout << "Base ouverte avec succes." << endl;
    return db;

} 

typedef struct _ctype
    : std::ctype<char>
{
    typedef std::ctype<char> base;
    static base::mask const* make_table(unsigned char space,
                                        base::mask* table)
    {
        base::mask const* classic(base::classic_table());
        std::copy(classic, classic + base::table_size, table);
        table[space] |= base::space;
        return table;
    }
    _ctype(unsigned char space)
    : base(make_table(space, table))
    {
    }
    base::mask table[base::table_size];
} _ctype;

/*
void import_into_base(sqlite3 *db){

    	char *err_msg=NULL;       // pointeur vers err
	char* req = ".import ../sqlite_toy/afile.dat CONTACTS;"; 
   	sqlite3_stmt *res;
	//int rc = sqlite3_prepare_v2(db, req, -1, &res, 0);
	int rc;
   	if (rc != SQLITE_OK){
		cout << "Erreur lors de la preparation de la requete." << endl;
	} 
	rc = sqlite3_exec(db, req, 0, 0, &err_msg);
	if (rc != SQLITE_OK){
		cout << "Erreur lors de l'ecriture:" << sqlite3_errmsg(db) << endl;
	}
	return rc;
}
*/

int insert_req (sqlite3 *db)
{
    	char *err_msg=NULL;       // pointeur vers err
	char* req = "INSERT INTO CONTACTS VALUES(666, \"Diable\", \"Mr\", \'M\', \"L'enfer\", \"15, Volcano St\", NULL, 66666, \"Orlando\", \"jesuisfou@hotmail.com\", NULL  )";
	int rc = sqlite3_exec(db, req, 0, 0, &err_msg);
	if (rc != SQLITE_OK){
		cout << "Erreur lors de l'ecriture:" << sqlite3_errmsg(db) << endl;
	}
	return rc;
}

int delete_req_params(
		sqlite3 *db,
		int idContact)
{

    	char *err_msg=NULL;       // pointeur vers err
	char* req = "DELETE FROM CONTACTS where IdContact = ?;";
   	sqlite3_stmt *res;
    	int rc = sqlite3_prepare_v2(db, req, -1, &res, 0);
    
    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_int(res, 1, idContact);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

	if (rc != SQLITE_OK){
		cout << "Failed to bind " << sqlite3_errmsg(db) << endl; 
	}
	// TODO: check why error code incorrect
	//
  	if (sqlite3_step(res) != SQLITE_OK) {
    		printf("\nCould not step (execute) stmt.\n");
    		return 1;
  	}
	
    	sqlite3_finalize(res);

	if (rc != SQLITE_OK){
		cout << "Erreur lors de l'ecriture:" << sqlite3_errmsg(db) << endl;
	}
	return rc;
}

int insert_req_params(
		sqlite3 *db,
		int idContact,
		char *nom, 
		char *prenom, 
		char *sexe,
		char *entreprise,
		char *adresse,
		char *complement, 
		char *code_postal,
		char *ville,
		char *email,
		char *date_naissance)
{

    	char *err_msg=NULL;       // pointeur vers err
	char* req = "INSERT INTO CONTACTS VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?  )";

   	sqlite3_stmt *res;

    	int rc = sqlite3_prepare_v2(db, req, -1, &res, 0);
    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_int(res, 1, idContact);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_text(res, 2, nom, strlen(nom), NULL);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_text(res, 3, prenom, strlen(prenom), NULL);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	cout << "Got there" << endl;
    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_text(res, 4, sexe, strlen(sexe), NULL);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}
	
    	cout << "Got there" << endl;
    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_text(res, 5, entreprise, strlen(entreprise), NULL);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_text(res, 6, adresse, strlen(adresse), NULL);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_text(res, 7, complement, strlen(complement), NULL);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_text(res, 8, code_postal, strlen(code_postal), NULL);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_text(res, 9, ville, strlen(ville), NULL);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_text(res, 10, email, strlen(email), NULL);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

    	if (rc == SQLITE_OK) {
        	rc = sqlite3_bind_text(res, 11, date_naissance, strlen(date_naissance), NULL);
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}

	if (rc != SQLITE_OK){
		cout << "Failed to bind " << sqlite3_errmsg(db) << endl; 
	}


  	if (sqlite3_step(res) != SQLITE_DONE) {
    		printf("\nCould not step (execute) stmt.\n");
    		return 1;
  	}
	
    	sqlite3_finalize(res);

	if (rc != SQLITE_OK){
		cout << "Erreur lors de l'ecriture:" << sqlite3_errmsg(db) << endl;
	}
	return rc;
}

void test_insert(sqlite3* db){
	
   	// open a file in read mode.
   	ifstream infile; 
   	infile.open("afile.dat"); 

	std::locale global;
	std::locale loc(global, new _ctype('|'));
    	infile.imbue(loc);

	int idContact;
	char nom[STR_NAME_SZ];
	char prenom[STR_NAME_SZ];
	char sexe;
	char entreprise[STR_NAMENT_SZ];
	char adresse[STR_RUE_SZ];
	char complement[STR_COMPLT_SZ]; 
	char code_postal[INT_CP_SZ];
	char ville[STR_VILLE_SZ];
	char email[255];
	char date_naissance[10];

   	cout << "Reading from the file" << endl; 
	cout << "Nom: " << endl;
	infile >> nom;
	cout << "Prenom:" << endl;
	infile >> prenom;
	cout << "Sexe" << endl;
	infile >> sexe;
	cout << "Entreprise" <<endl;
   	infile >> entreprise;
	cout << "Adresse" << endl;
	infile >> adresse;
	cout << "Complement" << endl;
	infile >> complement;
	cout << "Code Postal" << endl;
	infile >> code_postal;
	cout << "Ville" << endl;
	infile >> ville ;
	cout << "Email" << endl;
	infile >> email;
	cout << "Date Naissance" << endl;
	infile >> date_naissance;
   	cout << "End Reading from the file" << endl; 

   	infile.close();
	cout << "Sexe: " << sexe << endl;
	insert_req_params(
		db,
		idContact,
		nom, 
		prenom, 
		sexe,
		entreprise,
		adresse,
		complement, 
		code_postal,
		ville,
		email,
		date_naissance);

   	// again read the data from the file and display it.

   	// close the opened file.
}

int insert_req_params_test(sqlite3 *db){
	int idContact = 666;
	char *nom = "Diable";
	char *prenom = "Mr";
	char *sexe = "M";
	char *entreprise = "L'enfer";
	char *adresse = "15, Volcano St";
	char *complement = "BozoLand";
	char *code_postal = "66666";
	char *ville = "Orlando";
	char *email = "jesuisfou@hotmail.com";
	char* date_naissance = "01-01-1988";
	insert_req_params(
		db,
		idContact,
		nom,
		prenom, 
		sexe,
		entreprise,
		adresse,
		complement, 
		code_postal,
		ville,
		email,
		date_naissance);
    
    return 0;
    
}

int select_req (sqlite3 *db){

    char *errmsg=NULL;       // pointeur vers err
    sqlite3_stmt *stmt=NULL;

    char* requete3 = "SELECT * from CONTACTS LIMIT 10;";
    if (sqlite3_prepare_v2(db, requete3, -1, &stmt, NULL)){
    	cout << "Dommage! " << endl;
    }

    int rc;             // code retour
    while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {
	int identifier = sqlite3_column_int(stmt, 0);
	const  char* nom =  (const char*)sqlite3_column_text(stmt, 1);
	const  char* prenom = (const char*)sqlite3_column_text(stmt, 2);
	const  char* sexe = (const char*)sqlite3_column_text(stmt, 3);
	const  char* entreprise = (const char*)sqlite3_column_text(stmt, 4);
        
	const  char* complement = (const char*)sqlite3_column_text(stmt, 5);
	const  char* cp = (const char*)sqlite3_column_text(stmt, 6);
	const  char* ville = (const char*)sqlite3_column_text(stmt, 7);
	const  char* mail = (const char*)sqlite3_column_text(stmt, 8);
	const  char* date = (const char*)sqlite3_column_text(stmt, 9);
        

	cout 	<< "Identifier: " << identifier << " ; "
		<< "Nom: " << nom << " ; "
	        << "Prenom: " << (prenom == NULL ? "NULL" : prenom) << " ; "
		<< "Sexe: " << (sexe == NULL ? "NULL" : sexe) << " ; "
	        << "Entreprise: " << ( entreprise == NULL ? "NULL" : entreprise )<< " ; " 
		<< "Complement: " << (complement == NULL ? "NULL" : complement ) << " ; "
		<< "Code Postal: " << (cp == NULL ? "NULL" : cp)  << " ; "
		<< " Ville: " << (ville == NULL ? "NULL" : ville )<< " ; "
		<< "Mail" << ( mail == NULL ? "NULL" : mail ) << " ; "
		<< "Date: " << (date == NULL ? "NULL" : date)  << " ; "	
		<< endl;
	
    }


    sqlite3_finalize(stmt);
    rc = sqlite3_close(db);

    if (rc != SQLITE_OK) {
        printf("ERREUR Open : %s\n", sqlite3_errmsg(db));
       return 1;
    }
    return 0;

}


int select_by_req(sqlite3 *db, const char *ville, const char *name){

    	char *errmsg=NULL;       // pointeur vers err
    	sqlite3_stmt *stmt = NULL;
    	const char *req = "select * from CONTACTS where Ville LIKE ? AND Nom LIKE ?;";
    	const char *req2 = "select * from CONTACTS where Ville LIKE ?;";
    	const char *req3 = "select * from CONTACTS where Nom LIKE ?;";
    	const char *req4 = "select * from CONTACTS;";

	const char *chosenReq = NULL;
    	int rc;             // code retour
			 
	if (ville != NULL and name != NULL){
		chosenReq = req;
	} else if ( ville != NULL){
		chosenReq = req2;
	} else if ( name != NULL){
		chosenReq = req3;
	} else {
		chosenReq = req4;
	}
	cout << "Chosen Req" << chosenReq << endl;

    	if ((rc = sqlite3_prepare_v2(db, chosenReq, -1, &stmt, NULL)) != SQLITE_OK){
    		cout << "Dommage! " << endl;
    	}

    	if (rc == SQLITE_OK && ville != NULL) {
        	rc = sqlite3_bind_text(stmt, 1, ville, strlen(ville), NULL);
		cout << "Bind ville " << ville << endl;
    	} else {
        	fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    	}
	
    	if (rc == SQLITE_OK && name != NULL) {
        	rc = sqlite3_bind_text(stmt, (ville == NULL ? 1 : 2), name, strlen(name), NULL);
		cout << "Bind nom " << name << endl;
    	} else {
        	fprintf(stderr, "Failed to bind parameter: %s\n", sqlite3_errmsg(db));
    	}
   // const  char* nom =  (const char*)sqlite3_column_text(stmt, 1);

    	while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW)
    	{
		int identifier = sqlite3_column_int(stmt, 0);
		const  char* nom =  (const char*)sqlite3_column_text(stmt, 1);
		const  char* prenom = (const char*)sqlite3_column_text(stmt, 2);
		const  char* sexe = (const char*)sqlite3_column_text(stmt, 3);
		const  char* entreprise = (const char*)sqlite3_column_text(stmt, 4);
        const  char* adresse = (const char*)sqlite3_column_text(stmt, 5);
		const  char* complement = (const char*)sqlite3_column_text(stmt, 6);
		const  char* cp = (const char*)sqlite3_column_text(stmt, 7);
		const  char* ville = (const char*)sqlite3_column_text(stmt, 8);
		const  char* mail = (const char*)sqlite3_column_text(stmt, 9);
		const  char* date = (const char*)sqlite3_column_text(stmt, 10);

		cout 	<< "Identifier: " << identifier << " ; "
			<< "Nom: " << nom << " ; "
	        	<< "Prenom: " << (prenom == NULL ? "NULL" : prenom) << " ; "
			<< "Sexe: " << (sexe == NULL ? "NULL" : sexe) << " ; "
	        	<< "Entreprise: " << ( entreprise == NULL ? "NULL" : entreprise )<< " ; " 
			<< "Complement: " << (complement == NULL ? "NULL" : complement ) << " ; "
			<< "Code Postal: " << (cp == NULL ? "NULL" : cp)  << " ; "
			<< " Ville: " << (ville == NULL ? "NULL" : ville )<< " ; "
			<< "Mail" << ( mail == NULL ? "NULL" : mail ) << " ; "
			<< "Date: " << (date == NULL ? "NULL" : date)  << " ; "	
			<< endl;
	
    	}


    	sqlite3_finalize(stmt);
    	rc = sqlite3_close(db);

    	if (rc != SQLITE_OK) {
        	printf("ERREUR Open : %s\n", sqlite3_errmsg(db));
       		return 1;
    	}
    	return 0;
}


int main()
{
    	string db_path = "../data/dbContacts.db";
    	sqlite3* db = open_db(db_path);
    	//select_req(db);
    	//insert_req_params_test(db);
	//char *nom = "Diable";
	//select_by_req(db, NULL, nom);
	//char *ville = "PARIS";
	//char *name = "LOUIS";
	//select_by_req(db, ville, name);
	delete_req_params(db, 666);
    return 0;
}
